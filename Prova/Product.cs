﻿namespace Prova;

public class Product
{
    public Guid Id { get; private set; }
    public string Name { get; private set; } = string.Empty;
    public decimal Price { get; private set; }

    public Product(Guid id, string name, decimal price)
    {
        Id = id;
        Name = name;
        Price = price;
    }

    private void CheckName()
    {
        if (Name == null || Name.Equals(string.Empty))
        {
            throw new Exception("Nome não pode ser nulo ou vazio");
        }
    }

    private void CheckPrice()
    {
        if (Price <= 0)
        {
            throw new Exception("Valor não pode ser zero ou negativo");
        }
    }

    public void ValidateProduct()
    {
        CheckName();
        CheckPrice();
    }
}
