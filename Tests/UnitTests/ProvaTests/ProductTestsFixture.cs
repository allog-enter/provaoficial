using Bogus;
using Prova;

namespace ProvaTests;

public class ProductTestsFixture
{
    public Product GenerateInvalidNameProduct()
    {
        var faker = new Faker();
        
        Guid id = faker.Random.Guid();
        string name = string.Empty;
        decimal price = faker.Random.Decimal(min: 0.01m);

        return new Product(id, name, price);
    }

    public Product GeneratePriceProduct(decimal priceProduct)
    {
        var faker = new Faker();
        
        Guid id = faker.Random.Guid();
        string name = "Nome do produto";
        decimal price = priceProduct;

        return new Product(id, name, price);
    }
}
