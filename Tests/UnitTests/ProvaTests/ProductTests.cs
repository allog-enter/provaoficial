using FluentAssertions;
using Prova;

namespace ProvaTests;

public class ProductTests : IClassFixture<ProductTestsFixture>
{
    private ProductTestsFixture _fixture;
    public ProductTests(ProductTestsFixture fixture)
    {
        _fixture = fixture;
    }

    [Fact(DisplayName = "Product Name quando for invalido deve retornar exception")]
    [Trait("Category", "Product Unit Tests")]
    public void ValidateProduct_WhenProductNameIsInvalid_ShouldThrowException()
    {
        //Arrange      
        Product product = _fixture.GenerateInvalidNameProduct();

        //Act
        Action act = () => product.ValidateProduct();

        //Assert
        Assert.Throws<Exception>(act);
    }

    [Theory(DisplayName = "Product Price quando for invalido deve retornar exception")]
    [Trait("Category", "Product Unit Tests")]
    [InlineData(0)]
    [InlineData(-9.90)]
    public void ValidateProduct_WhenPriceIsInvalid_ShouldThrowException(decimal invalidPrice)
    {
        //Arrange      
        Product product = _fixture.GeneratePriceProduct(invalidPrice);

        //Act
        Action act = () => product.ValidateProduct();

        //Assert
        Assert.Throws<Exception>(act);
    }
}